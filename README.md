# Lesson 9

The `node_modules` folder is not included in the repository. In order to start correctly this code you have to first install modules. To do so, run: 

```bash
npm install
```

from within the project folder.


### Instructions to create a new express project

Start the project

```
npm init -y
```

Add the express module

```
npm install express
```

Create the index.js file

```
touch index.js
```

Add the following content to the index.js file

```js
const express = require('express')
const app = express()

app.get('/', (req,res)=> {
    return res.send("Hello world")
})

app.listen(3000)
```