// Importing all the modules we will need in our project
const express = require('express')
const path = require('path')

// Creating the application server from express
const app = express()

// Defining a new endpoint, called /api/live which will work
// with an HTTP GET method
app.get("/api/live", (req, res) => {
    const date = new Date()
    return res.json({
        date
    })
})

// Telling express that whatever else should be taken from 
// the client folder, if there is something there
app.use(express.static(path.join(__dirname, "client")))

// Starting the application server on the port 3000
app.listen(3000, () => {
    console.log("Listening on port 3000")
})